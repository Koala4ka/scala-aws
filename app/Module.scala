import com.google.inject.AbstractModule
import dao.ResidentDAO
import dao.impl.ResidentDAOImpl

class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[ResidentDAO]).to(classOf[ResidentDAOImpl])
  }
}

