package controllers

import dao.impl.ResidentDAOImpl
import play.api.libs.json.Json

import javax.inject._
import play.api.mvc._

import scala.concurrent.ExecutionContext


@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               residentDAO: ResidentDAOImpl)(implicit ex: ExecutionContext)
  extends AbstractController(cc) {

  def hello(name: String): Action[AnyContent] = Action {
    Ok(s"Hello, $name")
  }

  def getALl: Action[AnyContent] = Action.async { implicit request =>
    residentDAO.getAll.map {
      arg => Ok(Json.toJson(arg))
    }
  }
}
