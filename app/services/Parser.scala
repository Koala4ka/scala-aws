package services

import dao.ResidentDAO
import model.Resident

import java.io.File
import scala.io.Source
import monix.execution.Scheduler.{global => scheduler}

import scala.concurrent.duration.DurationInt

class Parser (residentDAO:ResidentDAO) {

  def fileReader(): Seq[Resident] = {
    val filePath = "app/resources/resident-samples.log"
    val file = new File(filePath)
    Source.fromFile(file).getLines()
      .map(s => {
        val fields = s.split(",")
        Resident(
          fields(0).trim.toLong,
          fields(1).trim,
          fields(2).trim.toDouble,
        )
      }).toSeq
  }
    scheduler.scheduleWithFixedDelay(1.minute, 2.minute) {
     fileReader().map(residentDAO.save)
    }

}
